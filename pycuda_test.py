import pycuda.autoinit
import pycuda.gpuarray as gpuarray
import pycuda.driver as cuda
import cv2
import numpy as np
import os
import time as time_measure

if os.system("cl.exe"):
    os.environ['PATH'] += ';'+r"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Tools\MSVC\14.28.29333\bin\Hostx64\x64"
if os.system("cl.exe"):
    raise RuntimeError("cl.exe still not found, path probably incorrect")

# video path
video = 'C:/Users/marti/Documents/Freeze_experiments_analysis/Frequency tests\\2019-08-30 Frequency test I - lower frequencies\\MVI_1222.MP4'
# skip these frames in the beginning of the video
start_frames = 7000
#analysis window
window_size = 10

start = cuda.Event()
end   = cuda.Event()

#load video and jump to a later frame
def load_video(video, frames):
    capture = cv2.VideoCapture(video)
    capture.set(cv2.CAP_PROP_POS_FRAMES, frames)
    return capture

def get_n_frames_array(a, b, n, capture):
    #read 100 frames - even and uneven frames splitted into two different arrays 
    for i in range(n):
        frames, img = capture.read()
        a[i] = img.astype(np.float32)
        b[i] = capture.read()[1].astype(np.float32)
    return a, b 
    
    
def gpu_calc(a, b, frames, window_size):
    a_gpu = gpuarray.to_gpu(a)
    b_gpu = gpuarray.to_gpu(b)

    #calculate on the gpu -> difference of subsequent frame and store the max difference between two pixels over that time window
    start.record()    
    a_b = (a_gpu - b_gpu).get()
    img = np.max(a_b * (255 / window_size), axis = 0)
    end.record() 
    end.synchronize()
    secs = start.time_till(end) * 1e-3
    print("GPU processing time = %fs" % (secs))
    cv2.imwrite(f'results/gpu_test_{frames}.jpg', img)   

def cpu_calc(a, b, frames, window_size):
    #calculate on the cpu -> difference of subsequent frame and store the max difference between each (normalized to the maximal brightness) pixel values over that time window
    start_time = time_measure.time()
    a_b = (a - b) 
    img = np.max(a_b * (255 / window_size), axis = 0)
    duration = time_measure.time() - start_time 
    print("CPU processing time = %fs" % (duration))
    cv2.imwrite(f'results/cpu_test_{frames}.jpg', img)    
def main():
    global start_frames, video, window_size
    capture = load_video(video, start_frames)
    for i in range(50):    #initialize two arrays for always subsequent frames
        a, b = np.zeros((window_size, 1080, 1920, 3)), np.zeros((window_size, 1080, 1920, 3))
        a, b = get_n_frames_array(a, b, window_size, capture)
        cpu_calc(a, b, start_frames + (i * window_size), window_size)
    for i in range(50):    #initialize two arrays for always subsequent frames
        a, b = np.zeros((window_size, 1080, 1920, 3)), np.zeros((window_size, 1080, 1920, 3))
        a, b = get_n_frames_array(a, b, window_size, capture)
        gpu_calc(a, b, start_frames + (i * window_size), window_size)
if __name__ == "__main__":
   main()
   
